#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import json
import datetime
from google.appengine.ext import ndb
from google.appengine.ext import db
from google.storage.speckle.proto.jdbc_type import NULL

html1 = """

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>eventForm</title>
        <meta name="author" content="ShivaKrishna" />
        <!-- Date: 2016-03-15 -->
    </head>
    <body>
    
    <form action="/addEvents/" method="post">
      Event Name:<br>
      <input type="text" name="eventname"><br>
      Event Description:<br>
      <input type="textarea" name="eventdesc"><br>
      Location:<br>
      <input type="text" name="location"><br>
      Posted by:<br>
      <input type="text" name="author"><br>
      StartDate:<br>
      <input type="text" name="Startdate"><br>
      EndDate:<br>
      <input type="text" name="Enddate"><br>
      StartTime:<br>
      <input type="text" name="Starttime"><br>
      EndTime:<br>
      <input type="text" name="Endtime"><br>
      <br>
      <input type="submit" value="Submit">
    </form>
    </body>
</html>
"""
html2 = """

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>postsForm</title>
        <meta name="author" content="ShivaKrishna" />
        <!-- Date: 2016-03-15 -->
    </head>
    <body>
    
    <form action="/addPosts/" method="post">
      Title :<br>
      <input type="text" name="post"><br>
      Short Description:<br>
      <input type="textarea" name="postdesc"><br>
      Posted By:<br>
      <input type="text" name="author"><br>
      Link:<br>
      <input type="text" name="link"><br>
      Category:<br>
      <input type="text" name="Category"><br>
      <br>
      <input type="submit" value="Submit">
    </form>
    </body>
</html>
"""

class ETable(db.Model):
    EventName = db.StringProperty()
    EventDescription = db.StringProperty()
    Location = db.StringProperty()
    Author = db.StringProperty()
    StartDate = db.DateTimeProperty()
    EndDate = db.DateTimeProperty()
#     Today = datetime.date.today()
#     Date= EndpointsDateTimeProperty(string_format='%m/%d/%y %H:%M')
    
class PTable(db.Model):
    Post = db.StringProperty()
    ShortDescription = db.StringProperty()
    Link = db.StringProperty()
    PostedBy = db.StringProperty()
    Category = db.StringProperty()
    PostedDate = db.DateTimeProperty()
    
class addEvents(webapp2.RequestHandler):
    def post(self):
        self.response.write('Inside AddEvents')
        ename = self.request.get("eventname")
        self.response.write(ename)
        edesc = self.request.get("eventdesc")
        self.response.write(edesc)
        location = self.request.get("location")
        self.response.write(location)
        author = self.request.get("author")
        self.response.write(author)
#         myDate= EndpointsDateTimeProperty(string_format='%m/%d/%y %H:%M')
#         date = datetime.datetime.now()
        x= self.request.get("Startdate")
        y= self.request.get("Starttime")
        a = x+" "+y
        date1 = datetime.datetime.strptime(a, "%m/%d/%Y %H:%M")
        self.response.write(date1)
        c= self.request.get("Enddate")
        d= self.request.get("Endtime")
        e = c+" "+d
        date2 = datetime.datetime.strptime(e, "%m/%d/%Y %H:%M")
        self.response.write(date2)
#         Today = today.DateTimeProperty()
#         self.response.write(Today)
        EvTab = ETable(EventName = ename, EventDescription = edesc, Location = location, Author = author, StartDate = date1, EndDate = date2)
        EvTab.put()
  
class addPosts(webapp2.RequestHandler):
    def post(self):
        self.response.write('Inside AddPosts')
        post = self.request.get("post")
        self.response.write(post)
        shortdesc = self.request.get("postdesc")
        self.response.write(shortdesc)
        link = self.request.get("link")
        self.response.write(link)
        postedBy = self.request.get("author")
        self.response.write(postedBy)
#         myDate= EndpointsDateTimeProperty(string_format='%m/%d/%y %H:%M')
#         date = datetime.datetime.now()
        category= self.request.get("Category")
        self.response.write(category)
        testDate = datetime.datetime.now()-datetime.timedelta(hours=6,seconds=0)
#         Today = today.DateTimeProperty()
#         self.response.write(Today)
        PsTab = PTable(Post = post, ShortDescription = shortdesc, Link = link, PostedBy = postedBy, Category = category, PostedDate = testDate)
        PsTab.put()
                
class getEvents(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        self.error = None
        finalResult= []
        ETable.EndDate._auto_now = False
        test = ETable()
        testDate = datetime.datetime.now()-datetime.timedelta(hours=6,seconds=0)
        persons = ETable.all().filter("EndDate <", testDate)
        for per in persons:
            per.delete()
        
        today=testDate.strftime("%m/%d/%Y %H:%M")
        
#          GqlQuery interface constructs a query using a GQL query string
#         q= ETable.filter("EndDate >=", testDate)
#         q = db.GqlQuery("SELECT * FROM ETable " +
#                         "WHERE EndDate >= :1" , testDate) 
#         q1=q.order(ETable.StartDate)
#         q.order("StartDate")             
                        
#         Query interface constructs a query using instance methods
        p = ETable.all()
        p1 = p.order("StartDate")
        #p2 = p1.filter("EndDate >=", testDate)
        #p3 = p2.order("EndDate")
        #p3 = p2.order("StartDate")
        #q=db.GqlQuery("SELECT * FROM ETable"
                      #+ "Order by StartDate")
            
        #r = ETable.all()
           
        #r.filter("EndDate >=", today)
        
        #seriazing datetime object
        date_handler = lambda obj: (
            obj.isoformat()
            if isinstance(obj, datetime.datetime)
            or isinstance(obj, datetime.date)
            or isinstance(obj, datetime.time)
            else None
            )
        
        for each in p1:
            resultList = dict()
            resultList['eventName'] = each.EventName
            resultList['eventDesc'] = each.EventDescription
            resultList['location'] = each.Location
            resultList['author'] = each.Author
            date1=each.StartDate.date()
            time1=each.StartDate.time()
            date2=each.EndDate.date()
            time2=each.EndDate.time()
#             date1=pickle.dumps(date)
#             time1=pickle.dumps(time)
            resultList['Startdate'] = date1
            resultList['Enddate'] = date2
            resultList['Starttime'] = time1
            resultList['Endtime'] = time2
            finalResult.append(resultList)
        self.response.write(json.dumps(finalResult,default=date_handler))

class getPosts(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        self.error = None
        finalResult= []
#         testDate = datetime.datetime.now()
#         today=testDate.strftime("%m/%d/%Y %H:%M")
        PS = PTable.all()
        PS2 = PS.order("-PostedDate")
        
        date_handler1 = lambda obj: (
            obj.isoformat()
            if isinstance(obj, datetime.datetime)
            or isinstance(obj, datetime.date)
            or isinstance(obj, datetime.time)
            else None
            )
            
        for each in PS2:
            resultList = dict()
            resultList['post'] = each.Post
            resultList['postdesc'] = each.ShortDescription
            resultList['link'] = each.Link
            resultList['author'] = each.PostedBy
            resultList['Category'] = each.Category
            
            date1=each.PostedDate.date()
            resultList['date'] = date1
            finalResult.append(resultList)
        self.response.write(json.dumps(finalResult,default=date_handler1))

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write(html1)
        self.response.write(html2)
app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/addEvents/', addEvents),
    ('/getEvents/', getEvents),
    ('/addPosts/', addPosts),
    ('/getPosts/', getPosts)
], debug=True)