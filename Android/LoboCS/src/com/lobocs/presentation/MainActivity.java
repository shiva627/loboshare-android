package com.lobocs.presentation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bvapp.arcraymenu.ArcMenu;
import com.lobocs.adapters.CustomPagerAdapter;
import com.lobocs.presentation.NetworkAwareActivity.NetworkUtil;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.format.DateFormat;


public class MainActivity extends NetworkAwareActivity /*implements OnRefreshListener*/{

	private static final int[] ITEM_DRAWABLES = {R.drawable.events3, R.drawable.fa, R.drawable.info1};

	private Context mContext;
	//	private SwipeRefreshLayout swipeView;
	boolean refreshToggle = true;
	StableArrayAdapter eventsStableAdapter;
	StableArrayAdapter postsStableAdapter;
	private static final String EVENTS_DATA_URL = "http://lobocsapp.appspot.com/getEvents/";
	private static final String POSTS_DATA_URL = "http://lobocsapp.appspot.com/getPosts/";

	public static final String TAG_EVENT_NAME = "eventName";
	public static final String TAG_EVENT_DESC = "eventDesc";
	public static final String TAG_AUTHOR = "author";
	public static final String TAG_LOCATION = "location";
	public static final String TAG_START_DATE = "Startdate";
	public static final String TAG_END_DATE = "Enddate";
	public static final String TAG_START_TIME = "Starttime";
	public static final String TAG_END_TIME = "Endtime";

	public static final String TAG_POST_NAME = "post";
	public static final String TAG_POST_DEC = "postdesc";
	public static final String TAG_CATEGORY = "Category";
	public static final String TAG_LINK = "link";
	public static final String TAG_POST_AUTHOR = "author";
	public static final String TAG_POST_POSTED_DATE = "date";

	static List<String> eventHeader = new ArrayList<String>();
	static List<String> postHeader = new ArrayList<String>();

	static HashMap<String, HashMap<String, String>> allEvents = new HashMap<String, HashMap<String,String>>();
	static HashMap<String, HashMap<String, String>> allPosts = new HashMap<String, HashMap<String,String>>();

	private ProgressDialog pDialog;

	ListView listview1;
	ListView listview2;
	Button btnInt;
	RelativeLayout rl;
	Button btnRefresh;
	RelativeLayout rlbtnstrip;

	@SuppressWarnings({ "deprecation", "deprecation" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.activity_main);

		rl = (RelativeLayout)findViewById(R.id.rlMain);
		rlbtnstrip = (RelativeLayout)findViewById(R.id.rlbtnstrip);

		btnRefresh = (Button)findViewById(R.id.button_Rfresh);

		//		rl.setBackgroundResource(R.drawable.grey_bg);
		rl.setBackgroundResource(R.drawable.layoutborder);


		int networkStatus = NetworkUtil.getConnectivityStatus(getApplicationContext());
		if(networkStatus != 0) {

			/*swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_view);
		swipeView.setOnRefreshListener(this);
		swipeView.setColorSchemeColors(Color.GRAY, Color.CYAN);
		swipeView.setDistanceToTriggerSync(20);// in dips
		swipeView.setSize(SwipeRefreshLayout.DEFAULT);*/


			listview1 = new ListView(mContext);
			listview2 = new ListView(mContext);
			listview1.setSelector(R.drawable.list_selector);
			listview2.setSelector(R.drawable.list_selector);

			//			listview1.setBackgroundColor(Color.parseColor("#D5D5D5"));
			//			listview1.setBackgroundDrawable(R.drawable.background_upper);
			//			listview1.setBackgroundResource(R.drawable.background_upper);

			Vector<View> pages = new Vector<View>();

			pages.add(listview1);
			pages.add(listview2);



			ViewPager vp = (ViewPager) findViewById(R.id.pager);
			CustomPagerAdapter adapter = new CustomPagerAdapter(mContext,pages);
			vp.setAdapter(adapter);

			new fetchDataFromServer().execute();


			listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					Object listObj = parent.getItemAtPosition(position);
					String clickedEvent = listObj.toString();
					Toast.makeText(getApplicationContext(), clickedEvent,Toast.LENGTH_SHORT).show();

					HashMap<String, String> hmapClickedEventDetails = allEvents.get(clickedEvent);
					System.out.println(allEvents.size());

					System.out.println(hmapClickedEventDetails.size());
					System.out.println(hmapClickedEventDetails.get("eName"));
					Bundle b = new Bundle();
					b.putSerializable("selectedEvent", hmapClickedEventDetails);
					Intent i = new Intent(getApplicationContext(),EventsDetailsActivity.class);
					i.putExtras(b);
					startActivity(i);
				}
			});


			listview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					Object listObj = parent.getItemAtPosition(position);
					String clickedPost = listObj.toString();
					Toast.makeText(getApplicationContext(), clickedPost,Toast.LENGTH_SHORT).show();

					HashMap<String, String> hmapClickedPostDetails = allPosts.get(clickedPost);
					System.out.println("sizeeee"+hmapClickedPostDetails.size());
					Bundle b = new Bundle();
					b.putSerializable("selectedPost", hmapClickedPostDetails);
					Intent i = new Intent(getApplicationContext(),PostsDetailsActivity.class);
					i.putExtras(b);
					startActivity(i);
				}
			});

			ArcMenu arcMenu = (ArcMenu)findViewById(R.id.arcMenu1);
			initArcMenu(arcMenu, ITEM_DRAWABLES);

		}
		else {
			try {
				AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();

				alertDialog.setTitle("Info");
				alertDialog.setMessage("Oops!! Internet not available, Please connect to any network and try again");
				alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						//						finish();
					}
				});
				alertDialog.show();
			}
			catch(Exception e)
			{
				Log.d("Show Dialog: ", e.getMessage());
			}
			btnInt = new Button(mContext);
			Vector<View> pages = new Vector<View>();

			//			btnInt.setText("Retry");
			//			btnInt.setLayoutParams(new LinearLayout.LayoutParams(10, 100));
			btnInt.setBackgroundResource(R.drawable.retry2);
			pages.add(btnInt);
			ViewPager vp = (ViewPager) findViewById(R.id.pager);
			CustomPagerAdapter adapter = new CustomPagerAdapter(mContext,pages);
			vp.setAdapter(adapter);

			btnInt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					finish();
					startActivity(getIntent());
				}
			});

			//			Toast.makeText(getApplicationContext(), "Oops!! Please connect to any network", Toast.LENGTH_SHORT).show();
		}


		btnRefresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				startActivity(getIntent());
			}
		});

		rlbtnstrip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				startActivity(getIntent());
			}
		});


	}


	/*private int getItem(int i) {
	       return mViewPager.getCurrentItem() + i;
	}*/

	class fetchDataFromServer extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Fetching Data ...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				eventHeader.clear();
				postHeader.clear();
				allEvents.clear();
				allPosts.clear();
				loadData();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(pDialog.isShowing())
			{
				pDialog.dismiss();
			}

			//			System.out.println("eventheader------------"+eventHeader.size());
			//			System.out.println("postHeader------------"+postHeader.size());

			eventsStableAdapter = new StableArrayAdapter(mContext, android.R.layout.simple_list_item_1, eventHeader);
			listview1.setAdapter(eventsStableAdapter);

			postsStableAdapter = new StableArrayAdapter(mContext, android.R.layout.simple_list_item_1, postHeader);
			listview2.setAdapter(postsStableAdapter);

		}
	}

	private void loadData(){
		try{
			JSONArray eventsDataJsonArray = getJSONfromURL(EVENTS_DATA_URL);
			JSONArray postsDataJsonArray = getJSONfromURL(POSTS_DATA_URL);

			SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yy");

			for(int i=0;i<eventsDataJsonArray.length();i++){
				JSONObject c = eventsDataJsonArray.getJSONObject(i);
				String eName = c.getString(TAG_EVENT_NAME);
				String eStrtDate = c.getString(TAG_START_DATE);

				String reformattedStrtDate = myFormat.format(fromUser.parse(eStrtDate));
				System.out.println(reformattedStrtDate);

				String eStrtTime = c.getString(TAG_START_TIME);

				//				eventHeader.add(eName+ "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t on \t" +eStrtDate+ "\t @ \t\t"+eStrtTime);
				//				String eHeader = eName+"-"+eStrtDate;
				//				String eHeader = eName+ "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t on \t" +eStrtDate+ "\t @ \t\t"+eStrtTime;

				String eHeader = eName +" - "+ reformattedStrtDate;
				eventHeader.add(eHeader);

				String eDesc = c.getString(TAG_EVENT_DESC);
				String eLocation = c.getString(TAG_LOCATION);
				String eAuthor = c.getString(TAG_AUTHOR);
				String eEndDate = c.getString(TAG_END_DATE);

				String reformattedEndDate = myFormat.format(fromUser.parse(eEndDate));

				String eEndTime = c.getString(TAG_END_TIME);

				HashMap<String, String> eventDetails = new HashMap<String, String>();

				if(eName.trim().length()==0){
					eventDetails.put("eName", "");
				}else{
					eventDetails.put("eName", eName);
				}

				if(eDesc.trim().length()==0){
					eventDetails.put("eDesc", "");
				}else{
					eventDetails.put("eDesc", eDesc);
				}

				if(eLocation.trim().length()==0){
					eventDetails.put("eLocation", "");
				}else{
					eventDetails.put("eLocation", eLocation);
				}

				if(eAuthor.trim().length()==0){
					eventDetails.put("eAuthor", "");
				}else{
					eventDetails.put("eAuthor", eAuthor);
				}

				if(eStrtDate.trim().length()==0){
					eventDetails.put("eStrtDate", "");
				}else{
					eventDetails.put("eStrtDate", reformattedStrtDate);
				}

				if(eEndDate.trim().length()==0){
					eventDetails.put("eEndDate", "");
				}else{
					eventDetails.put("eEndDate", reformattedEndDate);
				}

				if(eStrtTime.trim().length()==0){
					eventDetails.put("eStrtTime", "");
				}else{
					eventDetails.put("eStrtTime", eStrtTime);
				}

				if(eEndTime.trim().length()==0){
					eventDetails.put("eEndTime", "");
				}else{
					eventDetails.put("eEndTime", eEndTime);
				}

				allEvents.put(eHeader, eventDetails);
			}

			System.out.println("eventheader pos111------------"+eventHeader.size());
			System.out.println("alleventsSize pos11111------------"+allEvents.size());


			for(int i=0;i<postsDataJsonArray.length();i++){
				JSONObject c = postsDataJsonArray.getJSONObject(i);
				String pName = c.getString(TAG_POST_NAME);
				String pPostedDate = c.getString(TAG_POST_POSTED_DATE);

				String reformattedPostDate = myFormat.format(fromUser.parse(pPostedDate));

				//				postHeader.add(pName);
				//				String pHeader = pName;
				String pHeader = pName+" - "+reformattedPostDate;
				postHeader.add(pHeader);

				String pDesc = c.getString(TAG_POST_DEC);
				String pCategory = c.getString(TAG_CATEGORY);
				String pLink = c.getString(TAG_LINK);
				String pAuthor = c.getString(TAG_POST_AUTHOR);

				HashMap<String, String> postDetails = new HashMap<String, String>();

				if(pName.trim().length()==0){
					postDetails.put("pName", "");
				}else{
					postDetails.put("pName", pName);
				}

				if(pDesc.trim().length()==0){
					postDetails.put("pDesc", "");
				}else{
					postDetails.put("pDesc", pDesc);
				}

				if(pCategory.trim().length()==0){
					postDetails.put("pCategory", "");
				}else{
					postDetails.put("pCategory", pCategory);
				}

				if(pLink.trim().length()==0){
					postDetails.put("pLink", "");
				}else{
					postDetails.put("pLink", pLink);
				}

				if(pAuthor.trim().length()==0){
					postDetails.put("pAuthor", "");
				}else{
					postDetails.put("pAuthor", pAuthor);
				}

				if(pPostedDate.trim().length()==0){
					postDetails.put("pPostedDate", "");
				}else{
					postDetails.put("pPostedDate", reformattedPostDate);
				}

				allPosts.put(pHeader, postDetails);
			}

			//			System.out.println("postHeader pos111------------"+postHeader.size());
			//			System.out.println("allPosts pos111------------"+allPosts.size());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void initArcMenu(ArcMenu menu, int[] itemDrawables){
		final int itemcount = itemDrawables.length;
		for(int i=0; i<itemcount;i++){
			ImageView item = new ImageView(this);
			item.setImageResource(itemDrawables[i]);
			final int position = i;
			menu.addItem(item, new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					//					Toast.makeText(MainActivity.this, "position:"+position, Toast.LENGTH_SHORT).show();
					openAnotherActivity(position);
				}
			});
		}
	}

	public void openAnotherActivity(int position){
		if(position == 0){
			Intent intent = new Intent();
			super.onResume();
			intent.setClass(MainActivity.this, EventsEntryActivity.class);
			startActivity(intent);
		}
		else if(position == 1){
			Intent intent = new Intent();
			super.onResume();
			intent.setClass(MainActivity.this, PostsEntryActivity.class);
			startActivity(intent);
		}else if(position == 2){
			Intent intent = new Intent();
			super.onResume();
			intent.setClass(MainActivity.this, InformationActivity.class);
			startActivity(intent);
		}
	}

	/*	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			if (refreshToggle) {
				refreshToggle = false;
				Toast.makeText(MainActivity.this, "Refreshing", Toast.LENGTH_SHORT).show();
			} else {
				refreshToggle = true;
				Toast.makeText(MainActivity.this, "Refreshing", Toast.LENGTH_SHORT).show();
			}

			swipeView.postDelayed(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),
							"list refreshed", Toast.LENGTH_SHORT).show();
					swipeView.setRefreshing(false);
				}
			}, 1000);
		};
	};

	@Override
	public void onRefresh() {
		swipeView.postDelayed(new Runnable() {
			@Override
			public void run() {
				swipeView.setRefreshing(true);
				handler.sendEmptyMessage(0);
			}
		}, 1000);
	}*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static JSONArray getJSONfromURL(String url){

		InputStream is = null;
		String result = "";
		JSONObject jsonObj = null;
		JSONArray jsonArray = null;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
		}
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
			//			Log.d("check json", ""+result);
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}
		try{
			//			Log.d("check json array", ""+result);
			//			jsonObj = new JSONObject(result);
			jsonArray = new JSONArray(result);
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}
		//Log.d("check json obj", ""+jsonObj);
		//		return jsonObj;
		return jsonArray;
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}
	}
}
