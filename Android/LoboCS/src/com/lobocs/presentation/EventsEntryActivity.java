package com.lobocs.presentation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import com.lobocs.presentation.NetworkAwareActivity.NetworkUtil;


import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class EventsEntryActivity extends NetworkAwareActivity {

	Calendar myCalendar = Calendar.getInstance();
	EditText etStartDate;
	EditText etEndDate;
	EditText etStartTime;
	EditText etEndTime;
	EditText eventName;
	EditText eventDescription;
	EditText eventLocation;
	EditText eventPostedBy;
	Button btnSubmitEventsForm;
	private static final String EVENTURL = "http://lobocsapp.appspot.com/addEvents/";
	private ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events_entry);

		init();

		View.OnClickListener showDatePicker = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final View vv = v;

				DatePickerDialog datePickerDialog = new DatePickerDialog(EventsEntryActivity.this, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						if (vv.getId() == R.id.dp_startDate){
							myCalendar.set(Calendar.YEAR, year);
							myCalendar.set(Calendar.MONTH, monthOfYear);
							myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							String myFormat = "MM/dd/yyyy"; //In which you need put here
							SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
							etStartDate.setText(sdf.format(myCalendar.getTime()));
						} 
						else {
							myCalendar.set(Calendar.YEAR, year);
							myCalendar.set(Calendar.MONTH, monthOfYear);
							myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							String myFormat = "MM/dd/yyyy"; //In which you need put here
							SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
							etEndDate.setText(sdf.format(myCalendar.getTime()));
						}
					}
				}, myCalendar
				.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
				myCalendar.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
				datePickerDialog.show();
			}
		};

		etStartDate.setOnClickListener(showDatePicker);
		etEndDate.setOnClickListener(showDatePicker);


		etStartTime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);
				TimePickerDialog mTimePicker;
				mTimePicker = new TimePickerDialog(EventsEntryActivity.this, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
						etStartTime.setText( selectedHour + ":" + selectedMinute);
					}
				}, hour, minute, true);//Yes 24 hour time
				mTimePicker.setTitle("Select Time");
				mTimePicker.show();
			}
		}); 

		etEndTime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);
				TimePickerDialog mTimePicker;
				mTimePicker = new TimePickerDialog(EventsEntryActivity.this, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
						etEndTime.setText( selectedHour + ":" + selectedMinute);
					}
				}, hour, minute, true);//Yes 24 hour time
				mTimePicker.setTitle("Select Time");
				mTimePicker.show();
			}
		}); 

		btnSubmitEventsForm.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View v) {

				int networkStatus = NetworkUtil.getConnectivityStatus(getApplicationContext());
				if(networkStatus != 0) {
					boolean b = validateEventsForm();
//					System.out.println(b);
					if(b){
						String ename = eventName.getText().toString();
						String edesc = eventDescription.getText().toString();
						String eloc = eventLocation.getText().toString();
						String epost = eventPostedBy.getText().toString();
						String estDate = etStartDate.getText().toString();
						String eendDate = etEndDate.getText().toString();
						String estTime = etStartTime.getText().toString();
						String eendTime = etEndTime.getText().toString();

						ArrayList<String> eventDetails = new ArrayList<String>();

						eventDetails.add(ename);
						eventDetails.add(edesc);
						eventDetails.add(eloc);
						eventDetails.add(epost);
						eventDetails.add(estDate);
						if(eendDate.trim().length()==0){
							eendDate = estDate;
						}
						eventDetails.add(eendDate);
						eventDetails.add(estTime);
						eventDetails.add(eendTime);

						new submitToServer().execute(eventDetails);
					}
				} else {
					Toast.makeText(getApplicationContext(), "Oops!! Please connect to any network", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}


	class submitToServer extends AsyncTask<ArrayList<String>, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EventsEntryActivity.this);
			pDialog.setMessage("Submitting Event Data ...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(ArrayList<String>... receivedEventData) {
			try {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(EVENTURL);

				ArrayList<String> eventData = receivedEventData[0];

				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);

					nameValuePairs.add(new BasicNameValuePair("eventname", eventData.get(0)));
					nameValuePairs.add(new BasicNameValuePair("eventdesc", eventData.get(1)));
					nameValuePairs.add(new BasicNameValuePair("location", eventData.get(2)));
					nameValuePairs.add(new BasicNameValuePair("author", eventData.get(3)));
					nameValuePairs.add(new BasicNameValuePair("Startdate", eventData.get(4)));
					nameValuePairs.add(new BasicNameValuePair("Enddate", eventData.get(5)));
					nameValuePairs.add(new BasicNameValuePair("Starttime", eventData.get(6)));
					nameValuePairs.add(new BasicNameValuePair("Endtime", eventData.get(7)));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					httpclient.execute(httppost);
					System.out.println("Submitted Successfully");
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(pDialog.isShowing())
			{
				pDialog.dismiss();
			}
			eventName.setText("");
			eventDescription.setText("");
			eventLocation.setText("");
			eventPostedBy.setText("");
			etStartDate.setText("");
			etEndDate.setText("");
			etStartTime.setText("");
			etEndTime.setText("");
			eventName.requestFocus();
			Toast.makeText(getApplicationContext(), "Great!! You have shared an event with the lobos :)", Toast.LENGTH_SHORT).show();
		}
	}

	private boolean validateEventsForm(){
		String strtDate = etStartDate.getText().toString();
		String endDate = etEndDate.getText().toString();
		if(endDate.trim().length()==0){
			if(eventName.getText().toString().trim().length() != 0 && 
					eventDescription.getText().toString().trim().length() != 0 &&
					eventLocation.getText().toString().trim().length() != 0 &&
					etStartDate.getText().toString().trim().length() != 0 &&
					etStartTime.getText().toString().trim().length() != 0 &&
					etEndTime.getText().toString().trim().length() != 0){
				return true;
			}
			else{
				Toast.makeText(EventsEntryActivity.this, "Oops!! Please fill all the required fields :(", Toast.LENGTH_SHORT).show();
				return false;
			}
		}
		else{
			if(isDateAfter(strtDate, endDate)){
				if(eventName.getText().toString().trim().length() != 0 && 
						eventDescription.getText().toString().trim().length() != 0 &&
						eventLocation.getText().toString().trim().length() != 0 &&
						etStartDate.getText().toString().trim().length() != 0 &&
						etStartTime.getText().toString().trim().length() != 0 &&
						etEndTime.getText().toString().trim().length() != 0){
					return true;
				}
				else{
					Toast.makeText(EventsEntryActivity.this, "Oops!! Required fields are missing :(", Toast.LENGTH_SHORT).show();
					return false;
				}

			}else{
				Toast.makeText(EventsEntryActivity.this, "Oops!! End date cannot be before start date :(", Toast.LENGTH_SHORT).show();
				return false;
			}
		}
	}

	@SuppressLint("SimpleDateFormat") public static boolean isDateAfter(String startDate,String endDate)
	{
		boolean b = false;
		SimpleDateFormat dfDate = new SimpleDateFormat("MM/dd/yyyy");
		try {
			try {
				if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
					b = true;// If start date is before end date
				} else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
					b = true;// If two dates are equal
				} else {
					b = false; // If start date is after the end date
				}
			} catch (java.text.ParseException e) {
				e.printStackTrace();
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return b;
		}
		return b;
	}

	private void init(){
		etStartDate = (EditText)findViewById(R.id.dp_startDate);
		etEndDate = (EditText)findViewById(R.id.dp_endDate);
		etStartTime = (EditText)findViewById(R.id.tp_startTime);
		etEndTime = (EditText)findViewById(R.id.tp_endTime);
		eventName = (EditText)findViewById(R.id.et_eventName);
		eventDescription = (EditText)findViewById(R.id.et_eventDescription);
		eventLocation = (EditText)findViewById(R.id.et_eventLocation);
		eventPostedBy = (EditText)findViewById(R.id.et_eventPostedBy);
		btnSubmitEventsForm = (Button)findViewById(R.id.btnSubmitEvent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.events_entry, menu);
		return true;
	}

	@Override
	public void onBackPressed()
	{
		Intent it = new Intent(EventsEntryActivity.this, MainActivity.class);
		it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(it);
//		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
