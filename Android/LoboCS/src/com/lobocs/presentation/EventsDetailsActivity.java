package com.lobocs.presentation;

import java.util.HashMap;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class EventsDetailsActivity extends ActionBarActivity {

	TextView tvEventName;
	EditText etEventDescription;
	TextView tvEventLocation;
	TextView tvEventStarts;
	TextView tvEventEnds;
	TextView tvEventPostedBy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events_details);

		init();

		Intent in = getIntent();
		final HashMap<String, String> hmapEvent = (HashMap<String, String>) in.getSerializableExtra("selectedEvent");

		tvEventName.setText(hmapEvent.get("eName"));
		tvEventLocation.setText(hmapEvent.get("eLocation"));
		tvEventPostedBy.setText(hmapEvent.get("eAuthor"));
		tvEventStarts.setText(hmapEvent.get("eStrtTime")+" on "+hmapEvent.get("eStrtDate"));
		tvEventEnds.setText(hmapEvent.get("eEndTime")+" on "+hmapEvent.get("eEndDate"));
		etEventDescription.setText(hmapEvent.get("eDesc"));
	}

	private void init(){
		tvEventName = (TextView)findViewById(R.id.tvREName);
		tvEventLocation = (TextView)findViewById(R.id.tvRELocation);
		tvEventPostedBy = (TextView)findViewById(R.id.tvREPostedBy);
		tvEventStarts = (TextView)findViewById(R.id.tvREStarts);
		tvEventEnds = (TextView)findViewById(R.id.tvREEnds);
		etEventDescription = (EditText)findViewById(R.id.etERDescription);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.events_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed()
	{
		Intent it = new Intent(EventsDetailsActivity.this, MainActivity.class);
		it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(it);
//		finish();
	}
}
