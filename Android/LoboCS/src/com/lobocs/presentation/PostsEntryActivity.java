package com.lobocs.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import com.lobocs.presentation.NetworkAwareActivity.NetworkUtil;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class PostsEntryActivity extends ActionBarActivity implements OnItemSelectedListener {

	EditText postName;
	EditText shortDescription;
	EditText postLink;
	EditText postedby;
	Spinner categoriesSpinner;
	Button submitPostsForm;
	static String pcategory;
	private ProgressDialog pDialog;
	private static final String POSTURL = "http://lobocsapp.appspot.com/addPosts/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_posts_entry);
		this.setTitle("New Post");

		init();
		categoriesSpinner.setOnItemSelectedListener(this);
		List<String> postsCategories = new ArrayList<String>();

		postsCategories.add("Arts");
		postsCategories.add("Free Stuff");
		postsCategories.add("Health");
		postsCategories.add("Jobs");
		postsCategories.add("Need help");
		postsCategories.add("Science");
		postsCategories.add("Technology");
		postsCategories.add("Other");
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(PostsEntryActivity.this, android.R.layout.simple_spinner_item, postsCategories);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		categoriesSpinner.setAdapter(dataAdapter);


		submitPostsForm.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean b = validatePostsForm();
//				System.out.println(b);
				int networkStatus = NetworkUtil.getConnectivityStatus(getApplicationContext());
				if(networkStatus != 0) {
					if(b){
						String pname = postName.getText().toString();
						String pdescription = shortDescription.getText().toString();
						String plink = postLink.getText().toString();
						String pby = postedby.getText().toString();

						ArrayList<String> postDetails = new ArrayList<String>();
						postDetails.add(pname);
						postDetails.add(pdescription);
						postDetails.add(plink);
						postDetails.add(pby);
						postDetails.add(pcategory);

						new submitPostToServer().execute(postDetails);
					}
				} else {
					Toast.makeText(getApplicationContext(), "Oops!! Please connect to any network", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	class submitPostToServer extends AsyncTask<ArrayList<String>, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(PostsEntryActivity.this);
			pDialog.setMessage("Submitting Event Data ...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(ArrayList<String>... receivedEventData) {
			try {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(POSTURL);

				ArrayList<String> postData = receivedEventData[0];

				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);

					nameValuePairs.add(new BasicNameValuePair("post", postData.get(0)));
					nameValuePairs.add(new BasicNameValuePair("postdesc", postData.get(1)));
					nameValuePairs.add(new BasicNameValuePair("link", postData.get(2)));
					nameValuePairs.add(new BasicNameValuePair("author", postData.get(3)));
					nameValuePairs.add(new BasicNameValuePair("Category", postData.get(4)));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					httpclient.execute(httppost);
					System.out.println("Submitted Successfully");
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(pDialog.isShowing())
			{
				pDialog.dismiss();
			}
			postName.setText("");
			shortDescription.setText("");
			postLink.setText("");
			postedby.setText("");
			postName.requestFocus();

			Toast.makeText(getApplicationContext(), "Great!! You have shared a post with the lobos :)", Toast.LENGTH_SHORT).show();
		}
	}

	private boolean validatePostsForm(){
		if(postName.getText().toString().trim().length()!= 0 && 
				shortDescription.getText().toString().trim().length() != 0){
			return true;
		}
		else{
			Toast.makeText(PostsEntryActivity.this, "Oops!! Please fill all the required fields", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	private void init(){
		postName = (EditText)findViewById(R.id.et_postName);
		shortDescription = (EditText)findViewById(R.id.et_shortDescription);
		postLink = (EditText)findViewById(R.id.et_postLink);
		postedby = (EditText)findViewById(R.id.et_PostedBy);
		categoriesSpinner = (Spinner)findViewById(R.id.spinnerCategory);
		submitPostsForm = (Button)findViewById(R.id.btnSubmitPost);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.posts, menu);
		return true;
	}

	@Override
	public void onBackPressed()
	{
		Intent it = new Intent(PostsEntryActivity.this, MainActivity.class);
		it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(it);
//		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		pcategory = parent.getItemAtPosition(position).toString();
		//		Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}
}
