package com.lobocs.presentation;

import java.util.HashMap;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class PostsDetailsActivity extends ActionBarActivity {

	TextView tvPostName;
	EditText etPostDescription;
	TextView tvPostLink;
	TextView tvPostDate;
	TextView tvPostPostedBy;
	TextView tvPostCategory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_posts_details);
		
		init();

		Intent in = getIntent();
		final HashMap<String, String> hmapPost = (HashMap<String, String>) in.getSerializableExtra("selectedPost");

		tvPostName.setText(hmapPost.get("pName"));
		tvPostLink.setText(hmapPost.get("pLink"));
		tvPostPostedBy.setText(hmapPost.get("pAuthor"));
		tvPostDate.setText(hmapPost.get("pPostedDate"));
		tvPostCategory.setText(hmapPost.get("pCategory"));
		etPostDescription.setText(hmapPost.get("pDesc"));
	}

	private void init(){
		tvPostName = (TextView)findViewById(R.id.tvRPName);
		tvPostLink = (TextView)findViewById(R.id.tvRPLink);
		tvPostPostedBy = (TextView)findViewById(R.id.tvRPPostedBy);
		tvPostDate = (TextView)findViewById(R.id.tvRPDate);
		tvPostCategory = (TextView)findViewById(R.id.tvRPCategory);
		etPostDescription = (EditText)findViewById(R.id.etRPDescription);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.posts_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed()
	{
		Intent it = new Intent(PostsDetailsActivity.this, MainActivity.class);
		it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(it);
//		finish();
	}
}
