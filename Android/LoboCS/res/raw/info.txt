<b>Help :</b>
<br><br>
<b>Events: </b><br>
** New Events and workshop conducted by any department of the University can be posted here.<br>
** There is no restriction on the type of events that can be posted.<br>
** Title Should be short and To the Point. Other details can be be filled out in the description field provided.<br>
** Enter "NA" in any fields that are not applicable for a particular event.<br>
** All timings uploaded will be considered as MST.<br>
<br>
<b>Feed:</b><br>
** Any interesting articles related to education, science, Technology, Health or anything that might be helpful to students can be posted here.<br>
** We request users to post responsibly i.e., only the information that is useful to other students in a constructive way.<br>
** Title Should be short and To the Point. Other details can be be filled out in the description field provided. Provide web link of the complete post wherever applicable.<br>
** Need Help section should be exclusively used to ask help from fellow lobos. Free Stuff section should be exclusively used to about any stuff that you want to donate to fellow lobos. Jobs Section should be exclusively used to post about known job opportunities.<br>
** Enter "NA" in any fields that are not applicable for a particular event.<br>
<br>
We believe that you will use the platform responsibly to help each other.<br>
More Features Coming soon...:)<br>

<br>
<b>Welcome to LoboShare and Happy Sharing..:)</b><br>